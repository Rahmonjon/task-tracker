import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title: string = 'Task-Tracker';

  constructor() { } // it runs when it initialize code.

  ngOnInit(): void { // if you put code here, it runs.
  } 
  toggleAddTask(){
    console.log('toggle');    
  }

}
